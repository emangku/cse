<?php
    // $serverName = "172.21.50.31";
    // $myDB = "Opr_SQI";
    // $conn = mssql_connect($serverName, 'sa','P@ssXXXw0rd');
    // $selected = mssql_select_db($myDB, $conn) or die("Couldn't open database $myDB"); 
    // if( $conn === false ) {
    //     die("test");
    // } 

    // $QTR_BY       = isset($_POST["QTR_BY"]) ? $_POST["QTR_BY"] : '';
    // $QST_ID       = isset($_POST["QST_ID"]) ? $_POST["QST_ID"] : '';
    // $QSTID = "QST_ID->2->".$QST_ID."|^|"."QTR_BY->2->".$QTR_BY."|^|";
    // $qry = "EXEC [SUMMARY_TRANS] '$QSTID','' ";
    // // print_r($qry);
    // $sql = MSSQL_Query($qry) or die ("MSSQL Error database query".mysql_error());   

    // $json = array();


    // while($fet_tbl = MSSQL_Fetch_Assoc($sql)) {
    //     $json[] = $fet_tbl;
    // } 
    // if($json != null){
    //     echo json_encode(array('status'=>true,'msg'=>'Success','data'=>$json));
    // }else{
    //     echo json_encode(array('status'=>true,'msg'=>'Failed','data'=>'null data'));
    // }

try {
    $serverName = "172.21.50.31, 1433";
    $connectionOptions = array(
        "Database" => "Opr_SQI",
        "Uid" => "sa", "PWD" => "P@ssXXXw0rd"
    );
    $conn = sqlsrv_connect($serverName, $connectionOptions);
    if ($conn == false) die(FormatErrors(sqlsrv_errors()));
    $QTR_BY = isset($_POST["QTR_BY"]) ? $_POST["QTR_BY"] : '';
    $QST_ID = isset($_POST["QST_ID"]) ? $_POST["QST_ID"] : '';
    $QSTID = "QST_ID->2->" . $QST_ID . "|^|" . "QTR_BY->2->" . $QTR_BY . "|^|";
    // $REG_CODE = $_POST['REG_CODE'];
    $sql = sqlsrv_query($conn, "EXEC [SUMMARY_TRANS] '$QSTID',''");
    $data = 0;
    $json = array();
    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
        $json[] = array(
            "QTR_ID" => $row["QTR_ID"],
            "QST_ID" => $row["QST_ID"],
            "QST_CAPTION" => $row["QST_CAPTION"],
            "QTR_STORE_CODE" => $row["QTR_STORE_CODE"],
            "QTR_STORE_NAME" => $row["QTR_STORE_NAME"],
            "REG_CODE" => $row["REG_CODE"],
            "QTR_DATE" => $row["QTR_DATE"],
            "QTR_BY" => $row["QTR_BY"],
            "QTR_USRMS_NAME" => $row["QTR_USRMS_NAME"],
            "AK_BY" => $row["AK_BY"],
            "AK_BY_NAME" => $row["AK_BY_NAME"],
            "TOTAL" => $row["TOTAL"],
            "QST_GRADE" => $row["QST_GRADE"],
        );
    }

    if ($json != null) {
        echo json_encode(array("status" => true, "msg" => "Success", "data" => $json));
    } else {
        echo json_encode(array("status" => false, "msg" => "Failed", "data" => null));
    }
} catch (Exception $e) {
    echo json_encode($e);
}
?>