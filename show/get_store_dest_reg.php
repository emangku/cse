<?php

    // $serverName = "172.21.50.31";
    // $myDB = "StoreDataBase";
    // $conn = mssql_connect($serverName, 'sa','P@ssXXXw0rd');
    // $selected = mssql_select_db($myDB, $conn) or die("Couldn't open database $myDB"); 
    // if( $conn === false ) {
    //     die("test");
    // } 
    // $REG_CODE = $_POST['REG_CODE'];

    // $qry = "declare @reg_code VARCHAR (10) SET @REG_CODE='$REG_CODE' SELECT DISTINCT A.REG_CODE,(A.REG_CODE+AREA_CODE+BU_INDEX) AS STORE_CODE,(A.REG_CODE+AREA_CODE+BU_INDEX)+' - ' +BU_NAME STORE,AK_NAME,BU_NAME STORE_NAME,Replace(REG_NAME, 'CABANG','') REG_NAME FROM RETSYS.DBO.TABLE_BUSS_UNIT_MASTER A WITH(NOLOCK) INNER JOIN RETSYS.DBO.REGIONAL_MASTER B WITH(NOLOCK) ON A.REG_CODE=B.REG_CODE WHERE A.REG_CODE = @REG_CODE AND BU_TYPE = 4 AND ST_ID IN (1,2) AND (BU_IP_ADDR <> '0.0.0.0' OR BU_IP_ADDR <> '0.0' OR BU_IP_ADDR <> '') AND LEFT(AREA_CODE,1) NOT IN (6,7,8)";
    // // echo $qry;
    // // $sql = MSSQL_Query("SELECT [BU_CODE],[BU_NAME] FROM [StoreDataBase].[dbo].[BUSS_UNIT_MASTER] WHERE REG_CODE='$REG_CODE'") or die(mysql_error());
    // $sql = MSSQL_Query($qry) or die(mysql_error());
    // $json = array();
    
    // while($fet_tbl = MSSQL_Fetch_Assoc($sql)) {
    //     $json[] = $fet_tbl;
    // } 

    // if($json != null){
    //     echo json_encode(array('status'=>true,'msg'=>'Success','data'=>$json));
    // }else{
    //     echo json_encode(array('status'=>false,'msg'=>'Failed','data'=>'null data'));
    // }

try {
    $serverName = "172.21.50.31, 1433";
    $connectionOptions = array(
        "Database" => "StoreDataBase",
        "Uid" => "sa", "PWD" => "P@ssXXXw0rd"
    );
    $conn = sqlsrv_connect($serverName, $connectionOptions);
    if ($conn == false) die(FormatErrors(sqlsrv_errors()));

    $REG_CODE = $_POST['REG_CODE'];
    $sql = sqlsrv_query($conn, "declare @reg_code VARCHAR (10) SET @REG_CODE='$REG_CODE' SELECT DISTINCT A.REG_CODE,(A.REG_CODE+AREA_CODE+BU_INDEX) AS STORE_CODE,(A.REG_CODE+AREA_CODE+BU_INDEX)+' - ' +BU_NAME STORE,AK_NAME,BU_NAME STORE_NAME,Replace(REG_NAME, 'CABANG','') REG_NAME FROM RETSYS.DBO.TABLE_BUSS_UNIT_MASTER A WITH(NOLOCK) INNER JOIN RETSYS.DBO.REGIONAL_MASTER B WITH(NOLOCK) ON A.REG_CODE=B.REG_CODE WHERE A.REG_CODE = @REG_CODE AND BU_TYPE = 4 AND ST_ID IN (1,2) AND (BU_IP_ADDR <> '0.0.0.0' OR BU_IP_ADDR <> '0.0' OR BU_IP_ADDR <> '') AND LEFT(AREA_CODE,1) NOT IN (6,7,8)");
    $data = 0;
    $json = array();
    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
        $json[] = array(
            "REG_CODE" => $row["REG_CODE"],
            "STORE_CODE" => $row["STORE_CODE"],
            "STORE" => $row["STORE"],
            "AK_NAME" => $row["AK_NAME"],
            "STORE_NAME" => $row["STORE_NAME"],
            "REG_NAME" => $row["REG_NAME"]
        );
    }
    
    if ($json != null) {
        echo json_encode(array("status" => true, "msg" => "Success", "data" => $json));
    } else {
        echo json_encode(array("status" => false, "msg" => "Failed", "data" => null));
    }
} catch (Exception $e) {
    echo json_encode($e);
}
?>