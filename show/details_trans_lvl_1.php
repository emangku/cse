<?php

// $serverName = "172.21.50.31";
// $myDB = "Opr_SQI";
// $conn = mssql_connect($serverName, 'sa','P@ssXXXw0rd');
// $selected = mssql_select_db($myDB, $conn) or die("Couldn't open database $myDB"); 
// if( $conn === false ) {
//     die("test");
// } 

// $STORE_CODE = isset($_POST['STORE_CODE']) ? $_POST['STORE_CODE'] : '';
// $qry = "EXEC [QST_TRANS_LVL_1] '$STORE_CODE'";
// // print_r($qry);
// $sql = MSSQL_Query($qry) or die("MSSQL Error database");
// $json = array();


// while($fet_tbl = MSSQL_Fetch_Assoc($sql)) {
//     $json[] = $fet_tbl;
// } 

// if($json != null){
//     echo json_encode(array('status'=>true,'msg'=>'Success','data'=>$json));
// }else{
//     echo json_encode(array('status'=>true,'msg'=>'Failed','data'=>'null data'));
// }


try {
    $serverName = "172.21.50.31, 1433";
    $connectionOptions = array(
        "Database" => "Opr_SQI",
        "Uid" => "sa", "PWD" => "P@ssXXXw0rd"
    );
    $conn = sqlsrv_connect($serverName, $connectionOptions);
    if ($conn == false) die(FormatErrors(sqlsrv_errors()));

    $STORE_CODE = isset($_POST['STORE_CODE']) ? $_POST['STORE_CODE'] : '';
    $sql = sqlsrv_query($conn, "EXEC [QST_TRANS_LVL_1] '$STORE_CODE'");
    $data = 0;
    $json = array();
    $vjson = object;
    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
        $json[] = array(
            "STORE_NAME" => $row["STORE_NAME"],
            "USRMS_NAME" => $row["USRMS_NAME"],
            "QTR_ID" => $row["QTR_ID"],
            "QST_ID" => $row["QST_ID"],
            "STORE_CODE" => $row["STORE_CODE"],
            "QTR_DATE" => $row["QTR_DATE"],
            "QTR_BY" => $row["QTR_BY"],
            "QST_CAPTION" => $row["QST_CAPTION"],
            "QST_NOTE_1" => $row["QST_NOTE_1"],
            "QST_NOTE_2" => $row["QST_NOTE_2"],
            "QST_NOTE_3" => $row["QST_NOTE_3"],
            "QST_NOTE_4" => $row["QST_NOTE_4"],
            "QST_NOTE_5" => $row["QST_NOTE_5"],
            "QST_LVL" => $row["QST_LVL"],
            "TOTALPOINT" => $row["TOTALPOINT"],
            "GRADE" => $row["GRADE"],
            "QST_CHECK" => $row["QST_CHECK"],
            "REG" => $row["REG"],
            "AREA" => $row["AREA"],
            "BU_INDEX" => $row["STORE_CODE"],
            "QST_GRADE_A" => $row["QST_GRADE_A"],
            "MIN_GRADE_A" => $row["MIN_GRADE_A"],
            "MAX_GRADE_A" => $row["MAX_GRADE_A"],
            "QST_GRADE_B" => $row["QST_GRADE_B"],
            "MIN_GRADE_B" => $row["MIN_GRADE_B"],
            "MAX_GRADE_B" => $row["MAX_GRADE_B"],
            "QST_GRADE_C" => $row["QST_GRADE_C"],
            "MIN_GRADE_C" => $row["MIN_GRADE_C"],
            "MAX_GRADE_C" => $row["MAX_GRADE_C"],
            "QST_GRADE_D" => $row["QST_GRADE_D"],
            "MIN_GRADE_D" => $row["MIN_GRADE_D"],
            "MAX_GRADE_D" => $row["MAX_GRADE_D"]
        );
    }
    if ($json != null) {
        echo json_encode(array("status" => true, "msg" => "Success", "data" => $json));
    } else {
        echo json_encode(array("status" => false, "msg" => "Failed", "data" => null));
    }
} catch (Exception $e) {
    echo json_encode($e);
}

?>