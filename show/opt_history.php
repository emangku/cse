<?php

// $serverName = "172.21.50.31";
// $myDB = "Opr_SQI";
// $conn = mssql_connect($serverName, 'sa','P@ssXXXw0rd');
// $selected = mssql_select_db($myDB, $conn) or die("Couldn't open database $myDB"); 
// if( $conn === false ) {
//     die("test");
// } 
// $qry = "EXEC [dbo].[QST_OPTIONS_HISTORY]";
// // print_r($qry);
// $sql = MSSQL_Query($qry) or die("MSSQL Error database");
// $json = array();


// while($fet_tbl = MSSQL_Fetch_Assoc($sql)) {
//     $json[] = $fet_tbl;
// } 

// if($json != null){
//     echo json_encode(array('status'=>true,'msg'=>'Success','data'=>$json));
// }else{
//     echo json_encode(array('status'=>true,'msg'=>'Failed','data'=>'null data'));
// }
try {
    $serverName = "172.21.50.31, 1433";
    $connectionOptions = array(
        "Database" => "Opr_SQI",
        "Uid" => "sa", "PWD" => "P@ssXXXw0rd"
    );
    $conn = sqlsrv_connect($serverName, $connectionOptions);
    if ($conn == false) die(FormatErrors(sqlsrv_errors()));

    $REG_CODE = $_POST['REG_CODE'];
    $sql = sqlsrv_query($conn, "EXEC [dbo].[QST_OPTIONS_HISTORY]");
    $data = 0;
    $json = array();
    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
        $json[] = array(
            "QST_ID" => $row["QST_ID"],
            "QST_CAPTION" => $row["QST_CAPTION"]
        );
    }

    if ($json != null) {
        echo json_encode(array("status" => true, "msg" => "Success", "data" => $json));
    } else {
        echo json_encode(array("status" => false, "msg" => "Failed", "data" => null));
    }
} catch (Exception $e) {
    echo json_encode($e);
}

?>